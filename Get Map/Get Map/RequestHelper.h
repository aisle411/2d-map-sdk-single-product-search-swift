//
//  RequestHelper.h
//  DemoForMediaSaturn
//
//  Created by Minxin Guo on 11/23/16.
//  Copyright © 2016 aisle411. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestHelper : NSObject {
    NSString *partnerID;
    NSString *partnerSecret;
    NSString *host;
}
- (NSString*)constructGetWebserviceURLString:(NSString*)method withParameters:(NSArray*)parameters;
@end
