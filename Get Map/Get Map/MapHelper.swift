//
//  MapHelper.swift
//  Get Map
//
//  Created by Minxin Guo on 3/22/17.
//  Copyright © 2017 aisle411. All rights reserved.
//

import UIKit

class MapHelper {
    private let storeId: Int
    private let mapURLString: String
    
    init(storeId: Int, mapURLString: String) {
        self.storeId = storeId
        self.mapURLString = mapURLString
    }
    
    func getMap(completion: @escaping (String) -> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let urlRequest = URLRequest(url: URL(string: mapURLString)!)
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            guard let response = response as? HTTPURLResponse, let data = data else {
                completion("error")
                return
            }
            if let lastUpdateDate = response.allHeaderFields["Last-Modified"] as? String {
                if let mapDate = UserDefaults.standard.value(forKey: "\(self.storeId)") as? String, mapDate == lastUpdateDate {
                    let existingMapFilePath = self.getMapFilePath()
                    completion(existingMapFilePath)
                } else {
                    UserDefaults.standard.setValue(lastUpdateDate, forKey: "\(self.storeId)")
                    let newMapFilePath = self.saveMapFilePath(data: data)
                    completion(newMapFilePath)
                }
            } else {
                // Handle "Last-Modified" doesn't exist
                completion("error")
            }
        }
        dataTask.resume()
    }
    
    private func getMapFilePath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let path = paths.first!
        let fileName = "\(storeId).imap"
        let filePath = path + "/" + fileName
        let fileManager = FileManager()
        if fileManager.fileExists(atPath: filePath) {
            return filePath
        } else {
            return ""
        }
    }
    
    private func saveMapFilePath(data: Data) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let path = paths.first!
        let fileName = "\(storeId).imap"
        let filePath = path + "/" + fileName
        do {
            let url = URL(fileURLWithPath: filePath)
            try data.write(to: url, options: .atomic)
            return filePath
        } catch let error {
            print(error)
            return ""
        }
    }
}
