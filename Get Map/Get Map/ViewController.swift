//
//  ViewController.swift
//  Get Map
//
//  Created by Minxin Guo on 3/22/17.
//  Copyright © 2017 aisle411. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    var mapController: MapController!
    var mapBundle: MapBundle!
    var mapParser: MapBundleParser!
    let requestHelper = RequestHelper()
    
    // MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set indicator
        indicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        indicator.color = UIColor.green
        indicator.center = mapView.center
        indicator.hidesWhenStopped = true
        view.addSubview(indicator)
    }
    
    // MARK: - Methods
    func buildParams(id: Int) -> [Any] {
        var params = [String]()
        params.append("retailer_store_id=\(id)")
        // Note: Should pass in location info here
        //        params.append("latitude=0.0")
        //        params.append("longitude=0.0")
        return params
    }
    
    func loadMap(id: Int, urlString: String) {
        let mapHelper = MapHelper(storeId: id, mapURLString: urlString)
        mapHelper.getMap { filePath in
            if filePath == "error" {
                DispatchQueue.main.async {
                    self.indicator.stopAnimating()
                }
                return
            }
            self.mapParser = MapBundleParser(pathToArchive: filePath)!
            self.mapBundle = self.mapParser.parse()
            
            DispatchQueue.main.async {
                self.mapController = MapController()
                self.mapController.mapBundle = self.mapBundle
                self.mapController.view.frame = self.mapView.bounds
                self.mapController.floorLevel = 1
                self.mapController.setZoomButtonsHidden(true)
                self.mapController.setCompassEnabled(false)
                self.mapController.logoPosition = AisleLogoRightBottomPosition
                self.mapView.addSubview(self.mapController.view)
                self.indicator.stopAnimating()
            }
        }
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let text = searchBar.text, let storeId = Int(text) else {
            return
        }
        let params = buildParams(id: storeId)
        let mapRequestURLString = requestHelper.constructGetWebserviceURLString("map", withParameters: params)
        indicator.startAnimating()
        loadMap(id: storeId, urlString: mapRequestURLString!)
    }
}
