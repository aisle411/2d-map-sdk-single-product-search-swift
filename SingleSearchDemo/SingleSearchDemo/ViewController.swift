//
//  ViewController.swift
//  SingleSearchDemo
//
//  Created by Hayden Mans on 6/22/17.
//  Copyright © 2017 Hayden Mans. All rights reserved.
//

import UIKit

struct Section {
    let item_location_id: Int
    let map_location_id: Int
    let item_sub_location_id: Int
    let aisle: String
    let section: String
}

struct Product {
    let synonym_id: Int
    let synonym_nm: String
    let name: String
    let sections: [Section]
}

func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

func convertProducts(text: String) -> [String: AnyObject]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}

extension Product {
    init?(json: [String:Any]){
        print(json)
        guard let name = json["name"] as? String,
            let synonym_id = json["synonym_id"] as? Int,
            let synonym_nm = json["synonym_nm"] as? String,
            let sectionsJSON = json["sections"] as? [AnyObject]
            else {
                return nil
        }
        var sections: [Section] = []
        for part in sectionsJSON {
            let section = Section(item_location_id: part["item_location_id"] as! Int, map_location_id: part["map_location_id"] as! Int,
                                  item_sub_location_id: part["item_sub_location_id"] as! Int, aisle: part["aisle"] as! String,
                                  section: part["section"] as! String)
            sections.append(section)
        }
        self.synonym_id = synonym_id
        self.synonym_nm = synonym_nm
        self.name = name
        self.sections = sections
    }
}

func MD5(string: String) -> Data {
    let messageData = string.data(using:.utf8)!
    var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
    
    _ = digestData.withUnsafeMutableBytes {digestBytes in
        messageData.withUnsafeBytes {messageBytes in
            CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
        }
    }
    
    return digestData
}

extension String  {
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
}

extension String
{
    func encodeUrl() -> String
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    func decodeUrl() -> String
    {
        return self.removingPercentEncoding!
    }
    
}


class ViewController: UIViewController, UISearchBarDelegate {

    @IBOutlet weak var mapView: UIView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        self.loadMap()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var productCallOutOverlay: ProductCalloutOverlay!
    
    var mapController: MapController!
    var mapBundle: MapBundle!
    var mapParser: MapBundleParser!
    var activeMap: Int = 0
    
    
    func loadMap(){
        self.activeMap = 1141489
        self.getStoreMap(storeId: self.activeMap)
        self.productCallOutOverlay = ProductCalloutOverlay()
        self.productCallOutOverlay.products = []
        self.productCallOutOverlay.showChevronInCallout = true
        self.productCallOutOverlay.delegate = self
    }
    
    func getStoreMap(storeId: Int){
        //1a. Check if map containing folder exists locally
        //1b. If folder doesn't exist, create it
        //2a. Check if map exists locally
        //2b. If it does, load it
        //2c. If it doesn't, download it and save it, then load it
        let documentsUrl:URL = FileManager.default.urls(for: .documentDirectory, in:
            .userDomainMask).first as URL!
        let paths:NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentsDirectory = paths.object(at: 0) as! NSString
        let folderPath = documentsDirectory.appendingPathComponent("StoredMaps")
        let fileManager = FileManager()
        if fileManager.fileExists(atPath: folderPath) {}//if directory exists, continue
        else { //else, try to create the directory
            print("Directory not found.")
            do {
                try FileManager.default.createDirectory(atPath: folderPath, withIntermediateDirectories: false,attributes:  nil)
            }   catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        let destinationFilePath =
            documentsDirectory.appendingPathComponent("StoredMaps/\(storeId).imap")
        let destinationFileUrl =
            documentsUrl.appendingPathComponent("StoredMaps/\(storeId).imap")
        print("\(destinationFilePath)")
        if fileManager.fileExists(atPath: destinationFilePath) {
            print("Found map file.")
            DispatchQueue.global().async {
                self.mapParser = MapBundleParser(pathToArchive: destinationFilePath)!
                self.mapBundle = self.mapParser.parse()
                DispatchQueue.main.async {
                    self.mapController = MapController()
                    self.mapController.mapBundle = self.mapBundle
                    self.mapController.view.frame = self.mapView.bounds //self.mapView.bounds
                    self.mapController.floorLevel = 1
                    self.mapController.setZoomButtonsHidden(true)
                    self.mapController.setCompassEnabled(false)
                    self.mapController.logoPosition = AisleLogoRightBottomPosition
                    self.mapView.addSubview(self.mapController.view)
                    //self.indicator.stopAnimating()
                }
            }
        } //if directory exists, do not download map file
        else {
            let func_nm = "map"
            let part_id = 71
            let secret = "(Cyp<r7MZ=8]=a"
            let base = "https://qa.aisle411.ws/webservices3/\(func_nm).php?"
            let lat = 38.6256740
            let long = -90.1892740
            let store_id = storeId
            var sendurl = ""
            let uappend = "\(func_nm)?latitude=\(lat)&longitude=\(long)&partner_id=\(part_id)&retailer_store_id=\(store_id)&\(secret)"
            let jumbleData = MD5(string:uappend)
            let jumble = jumbleData.map { String(format: "%02hhx", $0) }.joined()
            let auth = "auth=\(jumble)"
            let finappend = "latitude=\(lat)&longitude=\(long)&partner_id=\(part_id)&retailer_store_id=\(store_id)&\(auth)"
            sendurl = base + finappend
            let url = URL(string: sendurl)
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:url!)
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    } catch {
                        print("Failed to copy file")
                    }
                    if (fileManager.fileExists(atPath: destinationFilePath)) {
                        DispatchQueue.global().async {
                            self.mapParser = MapBundleParser(pathToArchive: destinationFilePath)!
                            self.mapBundle = self.mapParser.parse()
                            DispatchQueue.main.async {
                                self.mapController = MapController()
                                self.mapController.mapBundle = self.mapBundle
                                self.mapController.view.frame = self.mapView.bounds //self.mapView.bounds
                                self.mapController.floorLevel = 1
                                self.mapController.setZoomButtonsHidden(true)
                                self.mapController.setCompassEnabled(false)
                                self.mapController.logoPosition = AisleLogoRightBottomPosition
                                self.mapView.addSubview(self.mapController.view)
                            }
                        }
                    }
                    else {
                        print("Error: Failed to load map downloaded from webservices.")
                    }
                }
            }
            task.resume()
        }
    }
    
    func singleProductSearch(storeId: Int, item: String){
        let func_nm = "searchproduct"
        let part_id = 71
        let secret = "(Cyp<r7MZ=8]=a"
        let base = "https://qa.aisle411.ws/webservices3/\(func_nm).php?"
        let upc = item.isNumber
        let beg = 0
        let end = 1
        let lat = 38.6256740
        let long = -90.1892740
        let store_id = storeId
        let term = item
        var sendurl = ""
        if (upc){
            let uappend = "\(func_nm)?end=\(end)&latitude=\(lat)&longitude=\(long)&partner_id=\(part_id)&retailer_store_id=\(store_id)&start=\(beg)&upc=\(upc)&\(secret)"
            let jumbleData = MD5(string: uappend)
            let jumble = jumbleData.map { String(format: "%02hhx", $0) }.joined()
            print("md5Hex: \(jumble)")
            let auth = "auth=\(jumble)"
            let finappend = "end=\(end)&latitude=\(lat)&longitude=\(long)&partner_id=\(part_id)&retailer_store_id=\(store_id)&start=\(beg)&upc=\(upc)&\(auth)"
            sendurl = base + finappend
            sendurl = sendurl.encodeUrl()
        }
        else {
            let tappend = "\(func_nm)?end=\(end)&latitude=\(lat)&longitude=\(long)&partner_id=\(part_id)&retailer_store_id=\(store_id)&start=\(beg)&term=\(term)&\(secret)"
            let jumbleData = MD5(string: tappend)
            let jumble = jumbleData.map { String(format: "%02hhx", $0) }.joined()
            print("md5Hex: \(jumble)")
            let auth = "auth=\(jumble)"
            let finappend = "end=\(end)&latitude=\(lat)&longitude=\(long)&partner_id=\(part_id)&retailer_store_id=\(store_id)&start=\(beg)&term=\(term)&\(auth)"
            sendurl = base + finappend
            sendurl = sendurl.encodeUrl()
        }
        let url = URL(string: sendurl)
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            if let data = data {
                let response = (NSString(data: data, encoding: String.Encoding.utf8.rawValue) ?? "No response from request.")
                print(response)
                var parsedData = convertToDictionary(text: response as String)
                var productsExists = false
                var closeExists = false
                if (parsedData != nil) {
                    productsExists = parsedData?["products"] != nil
                    closeExists = parsedData?["product_suggestions"] != nil
                }
                var products: [FMProduct] = []
                if (productsExists){
                    let obj = convertProducts(text: response as String)
                    let max = obj!["products"]!.count
                    var count = 0
                    while (count < max!){
                        let product = Product(json: obj!["products"]![0] as! [String: Any])
                        for section in product!.sections {
                            let temp = FMProduct()
                            let tempsection = FMSection(sublocation: Int32(section.item_sub_location_id), location: Int32(section.item_location_id))
                            temp.name = product!.name
                            temp.idn = Int32(product!.synonym_id)
                            let sectionlist : [String] = []
                            temp.sections = sectionlist
                            tempsection!.maplocation = Int32(section.map_location_id)
                            tempsection!.aisleTitle = section.aisle
                            tempsection!.title = section.section
                            temp.sections.append(tempsection!)
                            products.append(temp)
                        }
                        count += 1
                    }
                    self.productCallOutOverlay.products = products
                    DispatchQueue.main.async {
                        self.mapController.remove(self.productCallOutOverlay)
                        self.mapController.add(self.productCallOutOverlay)
                    }
                }
                else if (closeExists){
                    let obj = convertProducts(text: response as String)
                    let max = obj!["product_suggestions"]!.count
                    var count = 0
                    while (count < max!){
                        let product = Product(json: obj!["product_suggestions"]![0] as! [String: Any])
                        for section in product!.sections {
                            let temp = FMProduct()
                            let tempsection = FMSection(sublocation: Int32(section.item_sub_location_id), location: Int32(section.item_location_id))
                            temp.name = product!.name
                            temp.idn = Int32(product!.synonym_id)
                            let sectionlist : [String] = []
                            temp.sections = sectionlist
                            tempsection!.sublocation = Int32(section.item_sub_location_id)
                            tempsection!.aisleTitle = section.aisle
                            tempsection!.title = section.section
                            temp.sections.append(tempsection!)
                            products.append(temp)
                        }
                        count += 1
                    }
                    self.productCallOutOverlay.products = products
                    DispatchQueue.main.async {
                        self.mapController.remove(self.productCallOutOverlay)
                        self.mapController.add(self.productCallOutOverlay)
                    }
                }
            }
            else {
                print(error ?? "no error?")
            }
        }
        task.resume()
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar){
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        if let search = searchBar.text {
            singleProductSearch(storeId: activeMap, item: search)
            searchBar.text = ""
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        searchBar.text = ""
    }
}

extension ViewController: CalloutOverlayDelegate {
    func calloutOverlay(_ overlay: CalloutOverlay!, didItemSelected item: OverlayItem!) {

    }
    
    func calloutOverlay(_ overlay: CalloutOverlay!, didItemDeselected item: OverlayItem!) {
        
    }
    
    func calloutOverlay(_ overlay: CalloutOverlay!, didItemReselected oldItem: OverlayItem!, with item: OverlayItem!) {

    }
}




