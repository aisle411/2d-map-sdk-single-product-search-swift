//
//  RequestHelper.m
//  DemoForMediaSaturn
//
//  Created by Minxin Guo on 11/23/16.
//  Copyright © 2016 aisle411. All rights reserved.
//

#import "RequestHelper.h"
#import <CommonCrypto/CommonDigest.h>

@implementation RequestHelper

-(id)init{
    NSString* userInfoPath = [[NSBundle mainBundle] pathForResource:@"Credentials" ofType:@"plist"];
    NSDictionary* userInfoDict = [[NSDictionary alloc] initWithContentsOfFile:userInfoPath];
    partnerID = userInfoDict[@"partnerID"];
    partnerSecret = userInfoDict[@"partnerSecret"];
    host = userInfoDict[@"host"];
    return self;
}

-(NSString*)constructGetWebserviceURLString:(NSString*)method withParameters:(NSArray*)parameters{
    if ([partnerID length] == 0) {
        @throw [NSException exceptionWithName:@"Empty partnerID" reason:@"partnerID in Credentials.plist must be specified" userInfo: nil];
    }
    if ([partnerSecret length] == 0) {
        @throw [NSException exceptionWithName:@"Empty partnerSecret" reason:@"partnerSecret in Credentials.plist must be specified" userInfo: nil];
    }
    if ([host length] == 0) {
        @throw [NSException exceptionWithName:@"Empty host" reason:@"host in Credentials.plist must be specified" userInfo:nil];
    }
    NSString* url = [NSString stringWithFormat:@"%@/%@", host, [self AddAuth:method withParameters:[parameters arrayByAddingObject:[NSString stringWithFormat:@"partner_id=%@", partnerID]]]];
    
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    return url;
}

#pragma mark - MD5 for authentication
-(NSString*)AddAuth:(NSString*)method withParameters:(NSArray*)parameters{
    //Step 1: Sort parameters
    NSArray* sortedResult = [parameters sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    //Step 2: Append sorted parameters one by one to the end of the request
    NSString* toHash = [method stringByAppendingString:@"?"];
    
    NSString* result = [method stringByAppendingString:@".php?"];
    
    for (int i = 0; i < [sortedResult count]; i++) {
        if (i > 0) {
            toHash = [toHash stringByAppendingString:@"&"];
            result = [result stringByAppendingString:@"&"];
        }
        toHash = [toHash stringByAppendingString:sortedResult[i]];
        result = [result stringByAppendingString:sortedResult[i]];
    }
    
    //Step 3: Add Partner Secret
    toHash = [toHash stringByAppendingString:[NSString stringWithFormat:@"&%@", partnerSecret]];
    
    
    //Step 4: Encrypt with MD5
    const char *cStr = [toHash UTF8String];
    
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);
    
    NSMutableString* hashed = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH *2];
    
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [hashed appendFormat:@"%02x", digest[i]];
    }
    
    //Step 5: Append generated authentication to the end of the request
    result = [[result stringByAppendingString:@"&auth="] stringByAppendingString:hashed];
    
    return result;
}

#pragma mark - device token related
static NSString *guid() {
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"GUID"]) {
        return [[NSUserDefaults standardUserDefaults] stringForKey:@"GUID"];
    } else {
        CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
        NSString *uuidString = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuid));
        CFRelease(uuid);
        [[NSUserDefaults standardUserDefaults]
         setObject:uuidString forKey:@"GUID"];
        return uuidString;
    }
}

static NSString *sha1(NSString *string) {
    NSData *dataToHash = [string dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char hashBytes[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1([dataToHash bytes], (unsigned int)[dataToHash length], hashBytes);
    
    char hashString[2 * CC_SHA1_DIGEST_LENGTH + 1] = {0};
    int i = 0;
    for (i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        snprintf(&hashString[i * 2], 3, "%02x", hashBytes[i]);
    }
    
    return [NSString stringWithUTF8String:hashString];
}

-(NSString *)deviceToken {
    return sha1(guid());
}
@end
